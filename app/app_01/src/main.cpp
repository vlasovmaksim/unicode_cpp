///
/// A Windows example of convertion UTF-8 string to UTF-16.
///

#include "stdafx.h"


int main(int argc, char** argv) {
  // Create a converter.
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;

  std::string tmp_u8 = u8"\u4f60\u597d";

  // Convert UTF-8 to UTF-16.
  std::wstring tmp_u16 = conversion.from_bytes(tmp_u8);

  // On Windows we should output UTF-16.
  std::wcout << tmp_u16 << std::endl;

  // Convert UTF-16 to UTF-8.
  std::string tmp_u8_2 = conversion.to_bytes(tmp_u16);

  return 0;
}
