#pragma once

class UnicodeConverter {
public:
  /// Convert UTF-16 to UTF-8.
  std::string convert_utf16_to_utf8(const std::wstring& input);

  /// Convert UTF-8 to UTF-16.
  std::wstring convert_utf8_to_utf16(const std::string& input);

private:
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;
};
