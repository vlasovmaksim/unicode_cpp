///
/// A Windows example of convertion UTF-8 to and from UTF-16.
///

#include "stdafx.h"
#include "unicode_converter.h"
#include <assert.h>


int main(int argc, char** argv) {
  std::string test_input_u8 = u8"\u4f60\u597d";

  // Create a converter.
  UnicodeConverter converter;

  std::wstring test_input_u16 = converter.convert_utf8_to_utf16(test_input_u8);

  assert(test_input_u16.c_str(), "你好".c_str());

  // On Windows we should output UTF-16.
  std::wcout << test_input_u16 << std::endl;

  return 0;
}
