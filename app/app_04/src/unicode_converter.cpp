#include "unicode_converter.h"

std::string UnicodeConverter::convert_utf16_to_utf8(
  const std::wstring& input) {
  return conversion.to_bytes(input);
}

std::wstring UnicodeConverter::convert_utf8_to_utf16(
  const std::string& input) {
  return conversion.from_bytes(input);
}
