///
/// A Linux example of input UTF-8 string and convertion to UTF-16.
///

#include "stdafx.h"


int main(int argc, char** argv) {
  // Create a converter.
   std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> conversion;
  
   std::string tmp_u8;

   std::cin >> tmp_u8;

   std::cout << tmp_u8 << std::endl;

   // Convert UTF-8 to UTF-16.
   std::u16string tmp_u16 = conversion.from_bytes(tmp_u8);

  return 0;
}
