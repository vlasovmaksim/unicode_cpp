///
/// A Windows example of input UTF-16 string and convertion to UTF-8.
///


#include "stdafx.h"


int main(int argc, char** argv) {
  // Create a converter.
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;

  std::wstring tmp_u16;

  std::wcin >> tmp_u16;

  // Convert UTF-16 to UTF-8.
  std::string tmp_u8 = conversion.to_bytes(tmp_u16);

  return 0;
}
