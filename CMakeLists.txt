cmake_minimum_required(VERSION 3.0)

# Define project name as the folder name.
get_filename_component(ProjectId ${CMAKE_CURRENT_LIST_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})


if(WIN32)
    message(STATUS "WIN32")

    if (MSVC)
        message(STATUS "MSVC")

        set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)

        # Use multi-processor compilation.
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Wall /MP") 

        # Set character set.
        add_definitions(-D_UNICODE -DUNICODE)

        # Customize Debug info.
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} /ZI")        
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} /Zi")
    else()
        # Use C++ 11 standart.
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

        # Use C 99 standart.
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
    endif()
elseif(UNIX)
    message(STATUS "UNIX")

    # Use C++ 11 standart.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

    # Add Debug info.
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -g")

    # Turn off optimization.
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -O0")

    # Use C 99 standart.
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")    
endif()

message(STATUS "CMAKE_CXX_FLAGS: " ${CMAKE_CXX_FLAGS})


set_property(GLOBAL PROPERTY USE_FOLDERS ON)


set(dir ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${dir}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${dir}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${dir}/bin")


add_subdirectory(app)
